import {createSlice} from '@reduxjs/toolkit';

const initialState = {coinsList: []};
const coinsSlice = createSlice({
  name: 'coinsList',
  initialState,
  reducers: {
    setCoinsList: (state, action) => {
      state.coinsList = action.payload;
    },
  },
});
export default coinsSlice.reducer;
export const {setCoinsList} = coinsSlice.actions;
