import {configureStore} from '@reduxjs/toolkit';
import coinsSliceReducer from './features/coinsSlice';
const store = configureStore({
  reducer: {coinsList: coinsSliceReducer},
  middleware: getDefaultMiddleware => getDefaultMiddleware(),
});
export default store;
