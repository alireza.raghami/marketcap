import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {scale, ScaledSheet} from 'react-native-size-matters';
import {colors} from '../../../utils/constants/theme';
export default function HomeHeader({navigation}) {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={() => navigation.openDrawer()}>
        <Image
          style={styles.draweIcon}
          source={require('../../../assets/homeHeader/drawerIcon.png')}
        />
      </TouchableOpacity>
      <Text>22222222s</Text>
      <View style={styles.rightHeader}>
        <Image
          style={styles.diamondIcon}
          source={require('../../../assets/homeHeader/diamond.png')}
        />
        <Image
          style={styles.searchIcon}
          source={require('../../../assets/homeHeader/search.png')}
        />
      </View>
    </View>
  );
}
const styles = ScaledSheet.create({
  container: {
    width: '100%',
    height: '70@vs',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: colors.white,
  },
  draweIcon: {
    width: '30@s',
    height: '30@vs',
    resizeMode: 'contain',
    left: '4@s',
  },
  rightHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    left: '10@s',
  },
  diamondIcon: {
    width: '20@s',
    height: '30@vs',
    resizeMode: 'contain',
  },
  searchIcon: {
    width: '65@s',
    height: '65@vs',

    resizeMode: 'contain',
  },
});
