import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {useSelector} from 'react-redux';
import HomeHeader from './component/HomeHeader';
import {colors} from '../../utils/constants/theme';
import CoinListScreen from '../coinList/CoinListScreen';
const HomeScreen = ({navigation}) => {
  const state = useSelector(state => state);

  return (
    <View style={styles.container}>
      <HomeHeader navigation={navigation} />
      <CoinListScreen />
      {/* <Text>hoiioiiiiopikl</Text> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
});

export default HomeScreen;
