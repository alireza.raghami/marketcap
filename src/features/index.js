import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import MyTabs from './tabNavigator/MainTabBar';
import {enableScreens} from 'react-native-screens';
enableScreens();
const Stack = createNativeStackNavigator();

function Nav() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          options={{
            title: '',
            headerShown: false,
          }}
          name="Main"
          component={MyTabs}
        />
        {/* <Stack.Screen
          options={{
            title: '',
            headerShown: false,
          }}
          name="HomeHeader"
          component={HomeHeader}
        /> */}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Nav;
