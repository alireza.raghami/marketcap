import React from 'react';
import {Dimensions} from 'react-native';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomeScreen from '../home/HomeScreen';
import SplashScreen from '../splash/SplashScreen';
import App from '../drawer/Drawer';
import {createDrawerNavigator} from '@react-navigation/drawer';

const Tab = createMaterialBottomTabNavigator();
const StackDrawer = createDrawerNavigator();
const StackHome = createNativeStackNavigator();
const windowWidth = Dimensions.get('window').width;
const DrawerPage = () => {
  return (
    <StackDrawer.Navigator
      drawerContent={(props, dispatch) => App(props, dispatch)}>
      <StackDrawer.Screen
        options={{
          title: '',
          headerShown: false,
          headerTintColor: '#fff',
          headerStyle: {backgroundColor: '#303030'},
          drawerStyle: {
            width: windowWidth - 150,
          },
        }}
        name="Home"
        component={HomeScreen}
      />
    </StackDrawer.Navigator>
  );
};
function MainApp() {
  return (
    <StackHome.Navigator>
      <StackHome.Screen
        options={{
          headerShown: false,
        }}
        name="Drwer"
        component={DrawerPage}
      />
    </StackHome.Navigator>
  );
}
function MyTabs() {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="Home"
        component={MainApp}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="home" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="SplashScreen"
        component={SplashScreen}
        options={{
          tabBarLabel: 'Updates',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="bell" color={color} size={26} />
          ),
        }}
      />
      {/* <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="account" color={color} size={26} />
          ),
        }}
      /> */}
    </Tab.Navigator>
  );
}
export default MyTabs;
